#include <windef.h>

#include <X11/Xlib.h>
#include <unistd.h>

static Display* dpy;
static int scr;
static Window root_window;

void WINAPI ProxyXInit() {
    dpy = XOpenDisplay(0);
    scr = XDefaultScreen(dpy);
    root_window = XRootWindow(dpy, scr);
}

void WINAPI ProxyXGetMousePos(int* x, int* y) {
    Window returned_window;

    int root_x, root_y, dummy;
    XQueryPointer(dpy, root_window, &returned_window, &returned_window, &root_x, &root_y, &dummy, &dummy, &dummy);
    *x = root_x;
    *y = root_y;
}

void WINAPI ProxyXSetMousePos(int x, int y) {
    XWarpPointer(dpy, None, root_window, 0, 0, 0, 0, x, y);
    XFlush(dpy);
}
