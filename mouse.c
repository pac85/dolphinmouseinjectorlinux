//==========================================================================
// Mouse Injector for Dolphin
//==========================================================================
// Copyright (C) 2019-2020 Carnivorous
// All rights reserved.
//
// Mouse Injector is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, visit http://www.gnu.org/licenses/gpl-2.0.html
//==========================================================================
#include <stdint.h>
#include <stdio.h>
#include <windows.h>
#include "mouse.h"
#include "./manymouse/manymouse.h"

int32_t xmouse, ymouse; // holds mouse input data (used for gamedrivers)

static POINT mouselock; // center screen X and Y var for mouse
static ManyMouseEvent event; // hold current mouse event
static uint8_t lockmousecounter = 0; // limit SetCursorPos execution

//DLL pointers
typedef void (WINAPI *XInitC)();
XInitC XInit;
typedef void (WINAPI *XGetMousePosC)(int* x, int* y);
XGetMousePosC XGetMousePos;
typedef void (WINAPI *XSetMousePosC)(int x, int y);
XSetMousePosC XSetMousePos;


uint8_t MOUSE_Init(void);
void MOUSE_Quit(void);
void MOUSE_Lock(void);
void MOUSE_Update(const uint16_t tickrate);

//==========================================================================
// Purpose: initialize manymouse and returns detected devices (0 = not found)
//==========================================================================
uint8_t MOUSE_Init(void)
{
    //Loads x wrapper
    HMODULE m = LoadLibrary("xhacks.dll.so");
    XInit = (XInitC)GetProcAddress(m, "XInit");
    XGetMousePos = (XGetMousePosC)GetProcAddress(m, "XGetMousePos");
    XSetMousePos = (XGetMousePosC)GetProcAddress(m, "XSetMousePos");
    if(!XInit) {puts("no XINIt"); exit(-1);}
    if(!XGetMousePos) {puts("no XGetMousePos"); exit(-1);}
    if(!XSetMousePos) {puts("no XSetMousePos"); exit(-1);}
    XInit();

	return (ManyMouse_Init() > 0);
}
//==========================================================================
// Purpose: safely quit manymouse
//==========================================================================
void MOUSE_Quit(void)
{
	ManyMouse_Quit();
}
//==========================================================================
// Purpose: update cursor lock position
//==========================================================================
void MOUSE_Lock(void)
{
    int tx, ty;
    XGetMousePos(&tx, &ty);
    mouselock.x = tx;
    mouselock.y = ty;
}
//==========================================================================
// Purpose: update xmouse/ymouse with mouse input
// Changed Globals: lockmousecounter, xmouse, ymouse, event
//==========================================================================
void MOUSE_Update(const uint16_t tickrate)
{
    int tx, ty;
    XGetMousePos(&tx, &ty);
    xmouse = tx - mouselock.x;
    ymouse = ty - mouselock.y;
    xmouse *= 2;
    XSetMousePos(mouselock.x, mouselock.y);
}
